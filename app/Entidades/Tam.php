<?php

namespace App\Entidades;

class Tam implements CompanyInterface
{
    public function getFlights($from, $to = null, $departure_date, $return_date = null, $price = null): array
    {
        $path = file_get_contents(base_path("app/Arquivos/tam.json"));
        $data = json_decode($path);
        $data = collect($data);

        //Ida
        $ida = $data->where('departure_airport', $from);
        if (!is_null($to)) {
            $ida = $ida->where('destination_airport', $to);
        }
        //$ida = $ida->where('date', $departure_date);
        if (!is_null($price)) {
            $ida = $ida->where('price', '<=', $price);
        }
        $resposta['departures'] = $ida->toArray();

        if (!is_null($return_date)) {
            //Retorno
            if (!is_null($to)) {
                $retorno = $data->where('departure_airport', $to);
            }
            $retorno = $retorno->where('destination_airport', $from);
            //$retorno = $retorno->where('date', $return_date);
            if (!is_null($price)) {
                $retorno = $retorno->where('price', '<=', $price);
            }
            $resposta['returns'] = $retorno->toArray();
        }

        return $resposta;
    }
}
