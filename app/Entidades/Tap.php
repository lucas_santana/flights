<?php

namespace App\Entidades;

class Tap implements CompanyInterface
{
    public function getFlights($from, $to = null, $departure_date, $return_date = null, $price = null): array
    {
        $path = file_get_contents(base_path("app/Arquivos/tap.xml"));
        $xml = simplexml_load_string($path);
        $data = json_encode($xml);
        $data = json_decode($data);
        $data = collect($data->row);

        //Ida
        $ida = $data->where('from_location', $from);
        if (!is_null($to)) {
            $ida = $ida->where('to_location', $to);
        }
        //$ida = $ida->where('date', $departure_date);
        if (!is_null($price)) {
            $ida = $ida->where('price', '<=', $price);
        }
        $resposta['departures'] = $ida->toArray();

        if (!is_null($return_date)) {
            //Retorno
            $retorno = $data->where('from_location', $to);
            if (!is_null($to)) {
                $retorno = $retorno->where('to_location', $from);
            }
            //$retorno = $retorno->where('date', $return_date);
            if (!is_null($price)) {
                $retorno = $retorno->where('price', '<=', $price);
            }
            $resposta['returns'] = $retorno->toArray();
        }

        return $resposta;
    }
}
