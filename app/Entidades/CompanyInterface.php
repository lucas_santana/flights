<?php

namespace App\Entidades;


interface CompanyInterface
{
    public function getFlights($from, $to = null, $departure_date, $return_date, $price):array;
}
