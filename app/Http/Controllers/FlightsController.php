<?php

namespace App\Http\Controllers;

use App\Entidades\Tam;
use App\Entidades\Tap;
use Illuminate\Http\Request;


class FlightsController extends Controller
{
    public function search(Request $request)
    {
        $dados = $request->validate([
            'from' => 'required|min:3',
            'to' => 'min:3',
            'departure_date' => 'required|date',
            'return_date' => 'date',
            'max_price' => 'integer'
        ]);

        $dados = collect($dados);


        $tam = new Tam();
        $retornoJson = $tam->getFlights($dados->get('from'), $dados->get('to'), $dados->get('departure_date'), $dados->get('return_date'), $dados->get('max_price'));

        $tap = new Tap();
        $retornoXML = $tap->getFlights($dados->get('from'), $dados->get('to'), $dados->get('departure_date'), $dados->get('return_date'), $dados->get('max_price'));


        $retorno['departures'] = array_merge($retornoJson['departures'], $retornoXML['departures']);
        $retorno['returns'] = array_merge($retornoJson['returns'] + $retornoXML['returns']);

        return $retorno;
    }
}
